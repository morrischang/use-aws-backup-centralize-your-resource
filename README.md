# Use AWS Backup to centralize and autoamte the backup of data in EBS and EFS

## Overview

AWS Backup is a fully managed backup service that makes it easy to centralize and automate the backup of data across AWS services in the cloud and on premises. With AWS Backup, you can manage any backup activities related to **RDS (excluded Aurora), Dynamo DB, EBS, Storage gateway, and EFS** in the same console; besides, it's also possible to customize your backup schedule and AWS Backup will automatically get it done for you.

AWS Backup provides a fully managed, policy-based backup solution, simplifying your backup management, and enabling you to meet your business and regulatory backup compliance requirements.

<p align="center">
    <img src="image/001-Backup-EBS-EFS.png" width="100%" height="100%">
</p>

## Scenario
EFS system is often used for file sharing betwwen EC2 instances, and EBS is also usually attached to EC2 instance, serving as a storage device. If there are only a few of them, it might not be hard to manage the backup. However, most of companies might need to deal with tens of thousands of backups in one day, which could be really inconvenient if we get it done one by one. Thus, AWS Backup can make it much easier by centralize and automate all the back up in a single console, saving your time.


## Step by step

### Create VPC

- In the **AWS Management Console**, on the **services** menu, click **VPC**.

- In the navigation pane, click **Your VPCs**, and then click **Create VPC**.

- In the **Create VPC** dialog box, use the following information:

  - Name tag: My Backup Lab VPC
  - CIDR block: 10.10.0.0/16
  - Leave the Tenancy value set to Default.
  - Click **Create**.

    <p align="center">
      <img src="image/002-Backup-EBS-EFS.png" width="100%" height="100%">
    </p>

### Create Public Subnets

- In the navigation pane, click **Subnets**.

- Click **Create Subnet**.

- In the Create Subnet dialog box, use the following information:

  - Name tag: Public Subnet
  - VPC: Select the VPC you created earlier (My Backup Lab VPC)
  - Availability Zone: us-east-1a
  - CIDR block: 10.10.1.0/24
  - Click **Create**.

    <p align="center">
      <img src="image/003-Backup-EBS-EFS.png" width="80%">
    </p>

### Configure the Internet Gateway

- In the navigation pane, click **Internet Gateways**.

- Click **Create Internet Gateway**.

- In the **Create Internet Gateway** dialog box, for the Name tag, type **Public Subnet IGW**.

    <p align="center">
      <img src="image/004-Backup-EBS-EFS.png" width="100%">
  </p>

- Click **Create**.

- Select the gateway that you just created, and click **Attached to VPC** near the top of the Management Console.

- In the **Attach to VPC** dialog box, for VPC, click **My Backup Lab VPC**.

- Click **Yes, Attach**.

### Create a Public Route Table

- In the navigation pane, click **Route Tables**.

- Click **Create Route Table**.

- In the Create Route Table dialog box, using the following values:

  - Name tag: Public Route Table
  - VPC: select My Backup Lab VPC
  - Click **Yes, Create**.

      <p align="center">
        <img src="image/005-Backup-EBS-EFS.png" width="100%">
    </p>

- Select the new route table, and then click the **Routes** tab in the lower pane.

- To add a new route, click **Edit routes**.

- Click **Add route**.

- For the new route, add the following values:

  - Destination: 0.0.0.0/0
  - Target: place your cursor in the text box, and then choose **Internet Gateway**.

    <p align="center">
      <img src="image/006-Backup-EBS-EFS.png" width="100%">
    </p>

  - Then select **Public Subnet IGW**.

    <p align="center">
      <img src="image/007-Backup-EBS-EFS.png" width="100%">
    </p>

- Click **Save routes**.

### Associate the Public Subnet with the Public Route Table

- Click the **Subnet Associations** tab in the lower pane.

- Click **Edit subnet associations**.

- Both your public and private subnet will be displayed in the Associations list. Select the subnet named **Public Subnet** to associate it with your route table.

  <p align="center">
      <img src="image/008-Backup-EBS-EFS.png" width="100%">
  </p>

- Click **Save**.

### Create two EC2 instances

- On the **Services** menu, click **EC2**.

- In the navigation pane, click **Instances** and then click **Launch Instance**.

- To launch a new instance, from the **Quick Start** menu, in the row for the second **Amazon Linux AMI 2018.03.0 (HVM)** which ami id is **ami-0756fbca465a59a30**, click **Select**.

- On the **Choose an Instance Type** page, select default (**t2.micro**) instance, click **Next: Configure Instance Details**.

- On the page, set the following parameters and accept the other default values:
  - Network: My Backup Lab VPC
  - Subnet:  Public Subnet
  - Auto-assign Public IP: Enable

- Click **Next: Add Storage**.

- On the **Add Storage** page, use the following values for the storage:
  - Size(GiB): 8
  - Volume Type: General Purpose SSD (GP2)
  - Delete on Termination: Enabled

- Click **Next: Tag Instance**.

- To label this instance as your Bastion server in the Management Console, for Key, type **Name**, for value, type **My lab instance**.

- Click **Next: Configure Security Group**.

- For **Assign a security group**, click the **Create a new security group** option, and then change its name to **Public Security Group**
And set the inbound rule:
  - SSH/Anywhere/0.0.0.0/0,::0
  - NFS/Anywhere/0.0.0.0/0,::0

<p align="center">
      <img src="image/009-Backup-EBS-EFS.png" width="100%">
  </p>

- Click **Review and Launch**.

- On the **Review Instance Launch** page, review the configuration for your instance. When done, click **Launch**.

- Click **Choose an existing key pair** that you already created (or create a new keypair), select the acknowledgment check box.

- Click **Launch Instances**.

- Scroll down and click **View Instances**.

- Wait until **My lab instance** shows 2/2 checks passed in the **Status Checks** column. This will take 3-5 minutes. Use the refresh icon at the top right to check for updates.

- To create the other one, simply repeate this step, and change the name to **My lab instance 2**.
  
### Create EFS system

- On the **Services** menu, click **EFS**.

- Click **create file system**.

- Select the VPC you just created. In **Create mount targets**, choose **us-east-1a** and the subnet you just created, and choose the same security group as the instances configured.

  <p align="center">
      <img src="image/010-Backup-EBS-EFS.png" width="100%">
  </p>

- And wait until it's available.

  <p align="center">
      <img src="image/011-Backup-EBS-EFS.png" width="100%">
  </p>

### Mount EFS to the EC2 instances

- Opem terminal or cmd in your computer.

- ssh into one of the EC2 instance. ( If you are using Windows os please use Putty to ssh into the instance, for more info please go here: [Connecting to Your Linux Instance from Windows Using PuTTY](https://docs.aws.amazon.com/console/ec2/instances/connect/putty) )

  <p align="center">
      <img src="image/012-Backup-EBS-EFS.png" width="100%">
  </p>

- Type the fllowing commands to mount EFS to the EC2:

 ```
   ## login as root user
   sudo su
   ## install the EFS mount helper
   yum install -y amazon-efs-utils
   ## create a directory
   mkdir efs
   ## mount EFS to the directory efs
   mount -t efs <file system id>:/ efs
 ```

  <p align="center">
      <img src="image/013-Backup-EBS-EFS.png" width="100%">
  </p>

  <p align="center">
      <img src="image/014-Backup-EBS-EFS.png" width="100%">
  </p>
  
- Create a directory and txt file in EBS.

```
 ## create a directory
 mkdir <Yourfoldername>
 cd <Yourfoldername>
 ## create a txt file
 touch <Yourtxtname.txt>
```

- Create a directory and txt file in EFS.

```
## leave the directory you just created in EBS and get into efs directory
cd ..
cd efs
## create a directory
mkdir <Yourfoldername>
cd <Yourfoldername>
## create a txt file
touch <Yourtxtname.txt>
```

  <p align="center">
      <img src="image/015-Backup-EBS-EFS.png" width="100%">
  </p>

- Follow the previous steps to connect to the other EC2 instance and mount EFS to it , and use **ls** command to check if the file just created exists inside.
  
### Create AWS on-demand backup to backup both files in EFS and EBS

- On the **Services** menu, click **AWS backup**.

- In the **Dashboard**, click **Create on-demand backup**.

- Select Resource type as EFS and copy-paste your EFS resource id.

- Leave everything as default and click **Create on-demand backup**.

  <p align="center">
    <img src="image/016-Backup-EBS-EFS.png" width="100%">
  </p>

- Follow the same steps but choose EBS as instance type and type its resource id to create another backup.
  
- In the **navigation pane**, click **Job**, and wait for the backup (It might take some time). Eventually it would be like this:

   <p align="center">
      <img src="image/017-Backup-EBS-EFS.png" width="100%">
  </p>

### Delete the existing files in EBS and EFS and restore them via AWS Backup

- Go back to the **EC2 instance** where you created a folder with a file in the **EBS**, and remove them with the following commands:

```
cd <Yourfoldername>
rm <Yourfilename>
cd .. 
sudo rmdir <Yourfoldername>
```

- Remove the file in **EFS**.

```
cd efs
cd <Yourfoldername>
rm <Yourfilename>
cd .. 
sudo rmdir <Yourfoldername>
```

- Go to the **AWS backup** console.

- In the **navigation pane**, click **Protected resources**.

- choose your **EBS volume**.

<p align="center">
      <img src="image/018-Backup-EBS-EFS.png" width="100%">
  </p>
  
- Select the **Recovery point id**, and click **Restore**.

  <p align="center">
      <img src="image/019-Backup-EBS-EFS.png" width="100%">
  </p>

- In **Restore backup**, set **volume size** as 1 GiB, select **us-east-1a** as availability zone, leave others as default and click **Restore backup**.

  <p align="center">
      <img src="image/020-Backup-EBS-EFS.png" width="100%">
  </p>

- Go to **Protected resources** again, and select your **EFS**.

  <p align="center">
      <img src="image/021-Backup-EBS-EFS.png" width="100%">
  </p>
- Select the **Recovery point id**, and click **Restore**.

  <p align="center">
      <img src="image/022-Backup-EBS-EFS.png" width="100%">
  </p>

- In **Settings**, choose **Restore to directory in source file system**, and click **Restore backup**.

  <p align="center">
      <img src="image/023-Backup-EBS-EFS.png" width="100%">
  </p>

- In the navigation pane, click **Jobs**, and click **Restore Jobs**. Wait for the jobs completed.

<p align="center">
      <img src="image/024-Backup-EBS-EFS.png" width="100%">
  </p>

### Attach back-up EBS volume to EC2 instance and check if backup was sccessfully created

- Copy the **Resource id** which you just created in **Restore jobs**.

- In **Service**, Select **EC2**.

- In the navigation pane, select **voulume**, and paste the **volume id** you just copied on the search bar.

<p align="center">
      <img src="image/025-Backup-EBS-EFS.png" width="100%">
  </p>

- Right click on the volume and select **Attach volume**.

- Type the **instance id** you want to attach.

- Click **Attach**.
  
- Open your terminal and ssh into the instnace you just attached on, and type the following commands:

```
sudo mkdir <YourFoldername>
mount /dev/xvdf1 <Yourfoldername> -t ext4
cd <Yourfoldername>/home/ec2-user
ls
```

> You should see the file you jusy created previously.

<p align="center">
      <img src="image/026-Backup-EBS-EFS.png" width="100%">
  </p>

### Check if the file in EFS was also backed up

- ssh into one instance and type the following commands:

```
cd efs
ls
```

<p align="center">
      <img src="image/027-Backup-EBS-EFS.png" width="80%">
  </p>

## Conclusion

Now, You have learned how to:

1. Mount a **EFS and a EBS** to an instances.

2. Create a backup of **EFS and EBS** via **AWS Backup**.

## Clean up

1. **EC2 instnaces**

2. **EBS volume (including the backup one)**
   
3. **EFS**

4. **VPC**

5. Go to **AWS Backup vaults** to delete both of the **recovery points**

## Reference

* [AWS Backup](https://docs.aws.amazon.com/zh_tw/aws-backup/latest/devguide/how-it-works.html)